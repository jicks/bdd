BDD::Context ctx;
BDD::Function nota(ctx, "!a");
BDD::Function a(ctx, "a");
BDD::Function c = a & nota;

if (c.IsSatisfiable())
	return FAIL;