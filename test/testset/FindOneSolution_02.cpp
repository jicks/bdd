BDD::Context ctx;
BDD::Function a(ctx, "a");
BDD::Function b(ctx, "b");
BDD::Function c(ctx, "c");
BDD::Solution s = (a & !b & c).FindOneSolution();
if (s.ToString() != "<a:1><b:0><c:1>")
	return FAIL;
if (s.GetState("a") == false)
	return FAIL;
if (s.GetState("b") == true)
	return FAIL;
if (s.GetState("c") == false)
	return FAIL;