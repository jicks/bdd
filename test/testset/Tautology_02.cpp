BDD::Context ctx;
BDD::Function a(ctx, "a");
BDD::Function b(ctx, "b");
BDD::Function c = ((a & b) | (!(a & b)));

if (!c.IsTautology())
	return FAIL;