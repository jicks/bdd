BDD::Context ctx;
BDD::Function a(ctx, "a");
BDD::Function b = a | (!a);

if (!b.IsTautology())
	return FAIL;