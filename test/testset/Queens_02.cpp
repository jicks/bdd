  int nQueens = 2;
  BDD::Context ctx;
  BDD::Function result;
  BDD::Function** x;

  /* Initialization of the chess board variable */
  x = new BDD::Function*[nQueens];
  for (int i = 0; i < nQueens; i++)
  {
    x[i] = new BDD::Function[nQueens];
    for (int j = 0; j < nQueens; j++)
    {
      std::stringstream ss;
      ss << "x(" << i << "," << j << ")";
      x[i][j] = BDD::Function(ctx, ss.str().c_str());
    }
  }

  /* Ensuring at most one queen by row, column and diagonals */
  for (int i = 0; i < nQueens; i++)
    for (int j = 0; j < nQueens; j++)
    {
      BDD::Function atMostOne;

      for (int l = 0; l < nQueens; l++)
        if (l != j)
          atMostOne &= !x[i][l];

      for (int k = 0; k < nQueens; k++)
        if (k != i)
          atMostOne &= !x[k][j];

      for (int k = 0; k < nQueens; k++)
        if (k != i && (j+k-i) >= 0 && (j+k-i) < nQueens)
          atMostOne &= !x[k][j+k-i];

      for (int k = 0; k < nQueens; k++)
        if (k != i && (j+i-k) >= 0 && (j+i-k) < nQueens)
          atMostOne &= !x[k][j+i-k];

      result &= x[i][j].Implies(atMostOne);
    }

  /* Ensuring at least one queen by row */
  for (int i = 0; i < nQueens; i++)
  {
    BDD::Function row;
    for (int j = 0; j < nQueens; j++)
      row |= x[i][j];
    result &= row;
  }

  if (result.SatCount() != 0)
	  return FAIL;