BDD::Context context;
// s = a | b
BDD::Function s = BDD::Function(context, "a") | BDD::Function(context, "b");
// r = b & c
BDD::Function r = BDD::Function(context, "b") & BDD::Function(context, "c");
// r = (b & c) | b = b
r = s.Replace("a", r);
// r = b | !b
r = r | BDD::Function(context, "!b");
    
if (!r.IsTautology())
	return FAIL;