BDD::Context ctx;
BDD::Function a(ctx, "a");
BDD::Function b(ctx, "b");
BDD::Function c = a | b;
if (c.SatCount() != 3)
	return FAIL;