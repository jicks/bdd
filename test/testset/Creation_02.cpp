BDD::Context ctx;
BDD::Function a(ctx, "a");
BDD::Function b(ctx, "b");
for (int n = 0; n < 10000; n++)
{
	a =  a | (!a) | b | (!b);
	a =  a | b | (!a);
}

if (!a.IsTautology())
	return FAIL;