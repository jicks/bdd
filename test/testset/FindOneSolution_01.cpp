BDD::Context ctx;
BDD::Function a(ctx, "a");
BDD::Function b(ctx, "b");
BDD::Solution s = (a & b).FindOneSolution();
if (s.ToString() != "<a:1><b:1>")
	return FAIL;
if (s.GetState("a") == false)
	return FAIL;
if (s.GetState("b") == false)
	return FAIL;