BDD::Context ctx;
BDD::Function a(ctx, "a");
BDD::Function b(ctx, "b");
BDD::Solution s = ((a ^ b) & (!a)).FindOneSolution();
if (s.ToString() != "<a:0><b:1>")
	return FAIL;
if (s.GetState("b") == false)
	return FAIL;