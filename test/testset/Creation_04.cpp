BDD::Context ctx;
BDD::Function a[1000];
a[0] = BDD::Function(ctx);
for (int n = 1; n < 1000; n++)
{
	a[n] = BDD::Function(ctx) & a[n - 1];
	if (a[n].SatCount() != 1)
		return FAIL;
	if ((!a[n]).SatCount() != (n + 1))
		return FAIL;
}