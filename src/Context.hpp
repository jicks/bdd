/*
 * File : Context.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include "Platform.hpp"

#ifndef LIBBDD_CONTEXT_HPP_
# define LIBBDD_CONTEXT_HPP_

namespace BDD
{
    namespace Internal
    {
        class Context_p;
    }

    class Function;

    /**
     * @class Context
     * @brief Represents one context. A context is a place where the data
     * associated to each function is stored.
     * @note This class is refcounted and encapsulate a reference on an object.
     * By using this class you always target the same object even when you copy
     * an instance of this class.
     */
    class BDD_API_PUBLIC Context
    {
        public:
            /**
             * @brief Create a new Context
             */
            Context();
            /**
             * @brief Create a new Context and specify a cache size limit.
             * @brief MaxMemorySize the maximum size of the cache.
             */
            Context(u64 MaxMemorySize);
            Context(const Context& Ctx);
            ~Context();
            Context& operator =(const Context& Sol);
            void* operator new(size_t size);
            void operator delete(void* ptr);
            void* operator new[](size_t size);
            void operator delete[](void* ptr);
            /**
             * @brief Test if the context is a valid context.
             */
            bool IsValid();

            /**
             * @brief Create a Function associated to one boolean variable.
             * @param Variable the name of the variable.
             * @return The create function.
             */
            Function CreateFunction(const char* Variable);

            Internal::Context_p* Raw();

        private:
            Internal::Context_p* _Context;
            u32* _Refcount;
    };
}

#endif
