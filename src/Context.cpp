/*
 * File : Context.cpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include <new>

#include "Context.hpp"
#include "Function.hpp"
#include "Platform.hpp"
#include "internal/Context_p.hpp"

namespace BDD
{
    Context::Context()
        : _Context(0),
          _Refcount(0)
    {
        _Context = new (std::nothrow) Internal::Context_p();
        if (_Context)
        {
            _Refcount = new u32();
            *_Refcount = 1;
        }
    }

    Context::Context(u64 MaxMemorySize)
        : _Context(0),
          _Refcount(0)
    {
        _Context = new (std::nothrow) Internal::Context_p(MaxMemorySize);
        if (_Context)
        {
            _Refcount = new u32();
            *_Refcount = 1;
        }
    }

    Context::Context(const Context& Ctx)
        : _Context(Ctx._Context),
          _Refcount(Ctx._Refcount)
    {
        if (_Refcount)
        {
            *_Refcount = *_Refcount + 1;
        }
    }

    Context::~Context()
    {
        if (_Refcount)
        {
            *_Refcount = *_Refcount - 1;
            if (*_Refcount == 0)
            {
                if (_Context)
                {
                    delete _Context;
                    _Context = 0;
                }
                delete _Refcount;
                _Refcount = 0;
            }
        }
    }

    Context& Context::operator =(const Context& Ctx)
    {
        if (_Refcount)
        {
            *_Refcount = *_Refcount - 1;
            if (*_Refcount == 0)
            {
                if (_Context)
                {
                    delete _Context;
                }
                delete _Refcount;
            }
        }
        _Context = Ctx._Context;
        _Refcount = Ctx._Refcount;
        if (_Refcount)
        {
            *_Refcount = *_Refcount + 1;
        }
        else
        {
            _Context = 0;
        }
        return *this;
    }

    void* Context::operator new(size_t size)
    {
        void* arr = malloc(size * sizeof(Context));
        memset(arr, 0, size * sizeof(Context));
        return arr;
    }

    void Context::operator delete(void* ptr)
    {
        if (ptr)
        {
            free(ptr);
        }
    }

    void* Context::operator new[](size_t size)
    {
        void* arr = malloc(size * sizeof(Context));
        memset(arr, 0, size * sizeof(Context));
        return arr;
    }

    void Context::operator delete[](void* p)
    {
        if (p)
        {
            free(p);
        }
    }

    bool Context::IsValid()
    {
        if (_Context && _Refcount)
        {
            return _Context->IsValid();
        }
        return false;
    }


    Function Context::CreateFunction(const char* Variable)
    {
        return Function(*this, Variable);
    }

    Internal::Context_p* Context::Raw()
    {
        return _Context;
    }
}
