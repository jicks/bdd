/*
 * File : Expression.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include "../Platform.hpp"

#ifndef LIBBDD_EXPRESSION_HPP_
# define LIBBDD_EXPRESSION_HPP_

# include "Bytecode.hpp"

namespace BDD
{
    namespace Internal
    {
        class Expression
        {
            public:
                Expression();
                ~Expression();

                Expression Eval(u32 Variable, bool Value);

                void Push(u32 Value);

                bool TryGetValue(bool* value) const;

            private:
                bool _Fixed;
                bool _Value;

                u32* _Stack;
                u32* _StackStart;
                u32 _StackSize;
                u32 _MaxStackSize;
        };
    }
}
#endif /* LIBBDD_EXPRESSION_HPP_ */
