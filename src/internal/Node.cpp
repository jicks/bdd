/*
 * File : Node.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include "Node.hpp"
#include "Context_p.hpp"

namespace BDD
{
    namespace Internal
    {
        const Node* Node::True = reinterpret_cast<Node*>(1);
        const Node* Node::False = reinterpret_cast<Node*>(0);

        const size_t Node::IdCst = 3;
        const size_t Node::IdTrue = 2;
        const size_t Node::IdFalse = 1;

        NodeId Node::Restrict(Context_p* Context, u32 Variable, bool Value)
        {
            u32 NodeVariable = POINTER_TO_U32(this->Variable);
            NodeId Id = Context->GetMemoryPool()->GetIdFromNode(this);
            // Used to restore the pointer
            size_t LocalOnFalseId = Context->GetMemoryPool()->GetIdFromNode(OnFalse);
            Node* LocalOnFalse = this->OnFalse;
            size_t LocalOnTrueId = Context->GetMemoryPool()->GetIdFromNode(OnTrue);

            if (NodeVariable > Variable)
            {
                return Id;
            }

            if (NodeVariable < Variable)
            {
                size_t OnTrueId;
                if (OnTrue <= reinterpret_cast<Node*>(1))
                {
                    OnTrueId = LocalOnTrueId;
                }
                else
                {
                    OnTrueId = OnTrue->Restrict(Context, Variable, Value);
                    LocalOnFalse = Context->GetMemoryPool()->GetNodeFromId(LocalOnFalseId);
                }

                size_t OnFalseId;
                if (LocalOnFalseId < Node::IdCst)
                {
                    OnFalseId = LocalOnFalseId;
                }
                else
                {
                    OnFalseId = LocalOnFalse->Restrict(Context, Variable, Value);
                }

                return Context->AddNode(NodeVariable, OnTrueId, OnFalseId);
            }

            // (this->Variable == Variable)
            if (Value)
            {
                if (OnTrue <= reinterpret_cast<Node*>(1))
                    return LocalOnTrueId;
                else
                    return OnTrue->Restrict(Context, Variable, Value);
            }
            else // (!Value)
            {
                if (OnFalse <= reinterpret_cast<Node*>(1))
                    return LocalOnFalseId;
                else
                    return  OnFalse->Restrict(Context, Variable, Value);
            }

        }
    }
}
