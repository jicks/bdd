/*
 * File : Function_p.hxx
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#ifndef LIBBDD_FUNCTION_P_HXX_
# define LIBBDD_FUNCTION_P_HXX_

namespace BDD
{
    namespace Internal
    {
        template <class Operator>
        Function_p Function_p::Apply(const Function_p& Func) const
        {
            return Function_p(_Context, Node::Apply<Operator>(_Context, _BddId, Func._BddId));
        }
    }
}

#endif /* !LIBBDD_FUNCTION_P_HXX_ */
