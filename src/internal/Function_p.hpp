/*
 * File : Function_p.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#ifndef LIBBDD_FUNCTION_P_HPP_
# define LIBBDD_FUNCTION_P_HPP_

#include <string>
#include <vector>

#include "../Platform.hpp"
#include "MemoryPool.hpp"
#include "Node.hpp"

namespace BDD
{
    namespace Internal
    {
        class Solution_p;
        class Context_p;

        /**
         * @class Function_p
         * @brief Internal representation of a boolean function.
         * @warning this should be used only by the libbdd.
         */
        class Function_p
        {
            public:
                Function_p(const Context_p* Context);
                Function_p(const Context_p* Context, const std::string& Variable);
                Function_p(const Context_p* Context, const Node* Node);
                Function_p(const Context_p* Context, const size_t NodeId);
                ~Function_p();

                Function_p* Clone() const;

                bool IsValid() const;

                bool IsTautology() const;
                bool IsSatisfiable() const;
                u32  SatCount() const;

                Solution_p* FindOneSolution() const;
                std::vector<Solution_p*> FindAllSolutions() const;

                template <class Operator>
                Function_p Apply(const Function_p& Func) const;
                Function_p Restrict(u32 Variable, bool Value) const;

                Function_p And(const Function_p& Func) const;
                Function_p Or(const Function_p& Func) const;
                Function_p Xor(const Function_p& Func) const;
                Function_p Not() const;
                Function_p LogicalExist(const std::string& Variable) const;
                Function_p LogicalExist(u32 Variable) const;
                Function_p Implies(const Function_p& Func) const;
                Function_p Equivalent(const Function_p& Func) const;
                Function_p Replace(const std::string& Variable, const Function_p& Func) const;
                Function_p Replace(u32 Variable, const Function_p& Func) const;

                void InplaceAnd(const Function_p& Func);
                void InplaceOr(const Function_p& Func);
                void InplaceXor(const Function_p& Func);
                void InplaceImplies(const Function_p& Func);
                void InplaceEquivalent(const Function_p& Func);
                void InplaceNot();

                Function_p operator &(const Function_p& Func) const;
                Function_p operator |(const Function_p& Func) const;
                Function_p operator ^(const Function_p& Func) const;
                Function_p operator !() const;

                Function_p& operator &=(const Function_p& Func);
                Function_p& operator |=(const Function_p& Func);
                Function_p& operator ^=(const Function_p& Func);

                bool operator ==(const Function_p& Func);
                bool operator !=(const Function_p& Func);

                std::string ToDot() const;

            private:
                Context_p* _Context;
                size_t _BddId;
        };

        Function_p True(Context_p* Ctx);
        Function_p False(Context_p* Ctx);
    }
}

#include "Function_p.hxx"

#endif /* LIBBDD_FUNCTION_P_HPP_ */
