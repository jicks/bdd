/*
 * File : Solution_p.cpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include <sstream>

#include "Solution_p.hpp"

namespace BDD
{
    namespace Internal
    {

        Solution_p::Solution_p()
            : _Variables()
        {
        }

        bool Solution_p::GetState(const std::string name) const
        {
            std::map<std::string, bool>::const_iterator it =
                _Variables.find(name);
            if (it != _Variables.end())
                return it->second;
            else
                return true;
        }
        void Solution_p::SetState(const std::string name, bool value)
        {
            _Variables[name] = value;
        }

        Solution_p* Solution_p::Clone() const
        {
            Solution_p* solution = new Solution_p();
            for (std::map<std::string, bool>::const_iterator it=_Variables.begin();
                    it!=_Variables.end();
                    ++it)
            {
                solution->SetState(it->first, it->second);
            }
            return solution;
        }

        std::string Solution_p::ToString() const
        {
            if (_Variables.empty())
                return "";
            std::stringstream ss;
            for (std::map<std::string, bool>::const_iterator it=_Variables.begin();
                    it!=_Variables.end();
                    ++it)
            {
                ss << "<" << it->first << ":" << it->second << ">";
            }
            return ss.str();
        }

    }
}
