/*
 * File : Context_p.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#define MAX_LOGICAL_FUNCTIONS (10)
#define MAX_LOGICAL_COMMUTATIVE (5)
#define MAX_OPERATOR_SIZE (2048)
#include <map>
#include <string>
#ifdef WITH_CPP11
# include <unordered_map>
#endif
#include <utility>

#include "../Platform.hpp"


#ifndef LIBBDD_CONTEXT_P_HPP_
# define LIBBDD_CONTEXT_P_HPP_

#include "MemoryPool.hpp"

namespace BDD
{
    namespace Internal
    {
        struct Node;
        typedef size_t NodeId;

        /**
         * @class Context_p
         * @brief Internal context.
         * @warning this should be used only by the libbdd.
         */
        class Context_p
        {
            public:
                /**
                 * @brief create a new internal context.
                 * @param MaxMemorySize indicate the maximum size of the cache.
                 * This is only an indication. If the value is ste to zero then
                 * the cache will never be flushed
                 */
                Context_p(u64 MaxMemorySize = 0);
                /**
                 * @brief destroy the context and free everything.
                 * @warning this method may take a long time.
                 */
                ~Context_p();
                /**
                 * @brief check if this context is a valid context.
                 * @return true if the context is valid false otherwise.
                 */
                bool IsValid();
                /**
                 * @brief Create a new id for an anonymous variable.
                 * @return the id of the variable.
                 */
                u32 GetVariableId();
                /**
                 * @brief Create a new id for a variable or return
                 * an existing id if the variaable has already been created.
                 * @param Name the name of the variable.
                 * @return the id of the variable.
                 */
                u32 GetVariableId(const std::string& Name);
                /**
                 * @brief Create a new node inside the MemoryPool and return the Id.
                 * this method check if the node has already been added before.
                 * @param VariableId the Id of the variable.
                 * @param Lhs the node that is associated with TRUE.
                 * @param Rhs the node that is associated with FALSE.
                 * @return the id of the new node or the id of an existing node
                 */
                u32 AddNode(u32 VariableId, NodeId Lhs, NodeId Rhs);
                /**
                 * @brief Get the name of the variable associated to the
                 * specified variable Id.
                 * @param Id the Id of the variable.
                 * @return The name of the variable.
                 */
                std::string GetVariableName(u32 Id) const;
                /**
                 * @brief get a pointer on the Memory Pool that store the nodes.
                 */
                MemoryPool* GetMemoryPool();
                /**
                 * @brief try to access to a previously computed result.
                 */
                NodeId GetCachedResult(NodeId Lhs, NodeId Rhs, u8 Op) const;
                /**
                 * @brief add a result to the cache.
                 */
                void SetCachedResult(NodeId Lhs, NodeId Rhs, u8 Op, NodeId Result);

            private:
                MemoryPool _MemoryPool;
#ifdef WITH_CPP11
                std::unordered_map<std::string, u32> _VariableId;
                std::unordered_map<u32, std::string> _VariableName;
                std::unordered_map<std::pair<u32, std::pair<NodeId, NodeId> >, NodeId> _NodeMap;
                std::unordered_map<std::pair<NodeId, NodeId>, NodeId>* _CachedOperations;
#else
                std::map<std::string, u32> _VariableId;
                std::map<u32, std::string> _VariableName;
                std::map<std::pair<u32, std::pair<NodeId, NodeId> >, NodeId> _NodeMap;
                std::map<std::pair<NodeId, NodeId>, NodeId>* _CachedOperations;
#endif
                u32 _NextId;
        };
    }
}

#endif /* !LIBBDD_CONTEXT_P_HPP_ */
