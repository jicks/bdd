/*
 * File : Function_p.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "../Platform.hpp"
#include "Context_p.hpp"
#include "Function_p.hpp"
#include "LogicalFunctions.hpp"
#include "Solution_p.hpp"

namespace BDD
{
    namespace Internal
    {
        Function_p::Function_p(const Context_p* Context)
            : _Context(const_cast<Context_p*>(Context)),
              _BddId(0)
        {
            u32 variable = _Context->GetVariableId();
            _BddId = _Context->AddNode(variable, Node::IdTrue, Node::IdFalse);
        }

        Function_p::Function_p(const Context_p* Context, const std::string& Variable)
            : _Context(const_cast<Context_p*>(Context)),
              _BddId(0)
        {
            std::string VariableNotConst = Variable;
            bool notVar = false;
            while (VariableNotConst.length() > 1 && VariableNotConst[0] == '!')
            {
                VariableNotConst = VariableNotConst.substr(1);
                notVar = !notVar;
            }
            u32 variable = _Context->GetVariableId(VariableNotConst);
            if (notVar)
                _BddId = _Context->AddNode(variable, Node::IdFalse, Node::IdTrue);
            else
                _BddId = _Context->AddNode(variable, Node::IdTrue, Node::IdFalse);
        }

        Function_p::Function_p(const Context_p* Context, const Node* NodePtr)
            : _Context(const_cast<Context_p*>(Context)),
              _BddId(0)
        {
            _BddId = _Context->GetMemoryPool()->GetIdFromNode(const_cast<Node*>(NodePtr));
        }

        Function_p::Function_p(const Context_p* Context, size_t NodeId)
            : _Context(const_cast<Context_p*>(Context)),
            _BddId(NodeId)
        {
        }

        Function_p* Function_p::Clone() const
        {
            return new Function_p(_Context, _BddId);
        }

        Function_p::~Function_p()
        {
            // FIXME
        }

        bool Function_p::IsValid() const
        {
            // FIXME
            return true;
        }

        static void FindOneSolution_Rec(Context_p* Context, Node* Node, Solution_p* Solution)
        {
            if (Node->OnTrue == Node::True)
                Solution->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), true);
            else if (Node->OnFalse == Node::True)
                Solution->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), false);
            else if (Node->OnTrue != Node::False)
            {
                Solution->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), true);
                FindOneSolution_Rec(Context, Node->OnTrue, Solution);
            }
            else if (Node->OnFalse != Node::False)
            {
                Solution->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), false);
                FindOneSolution_Rec(Context, Node->OnFalse, Solution);
            }
        }

        Solution_p* Function_p::FindOneSolution() const
        {
            Node* bdd = _Context->GetMemoryPool()->GetNodeFromId(_BddId);
            Solution_p* sol = new Solution_p();
            if (bdd->Solutions == 0)
                return sol;
            FindOneSolution_Rec(_Context, bdd, sol);
            return sol;
        }

        static void FindAllSolutions_Rec(Context_p* Context, Node* Node, Solution_p* Solution, std::vector<Solution_p*>& Solutions)
        {
            if (Node->OnTrue == Node::True)
            {
                if (Node->OnFalse == Node::True)
                {
                    Solution_p* Duplicate = Solution->Clone();
                    Duplicate->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), true);
                    Solutions.push_back(Duplicate);
                    Solution->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), false);
                    Solutions.push_back(Solution);
                }
                else if (Node->OnFalse != Node::False)
                {
                    Solution_p* Duplicate = Solution->Clone();
                    Duplicate->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), true);
                    Solutions.push_back(Duplicate);
                    Solution->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), false);
                    FindAllSolutions_Rec(Context, Node->OnFalse, Solution, Solutions);
                }
                else
                {
                    Solution->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), true);
                    Solutions.push_back(Solution);
                }
            }
            else if (Node->OnFalse == Node::True)
            {
                if (Node->OnTrue != Node::False)
                {
                    Solution_p* Duplicate = Solution->Clone();
                    Duplicate->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), false);
                    Solutions.push_back(Duplicate);
                    Solution->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), true);
                    FindAllSolutions_Rec(Context, Node->OnTrue, Solution, Solutions);
                }
                else
                {
                    Solution->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), false);
                    Solutions.push_back(Solution);
                }
            }
            else if (Node->OnTrue != Node::False)
            {
                if (Node->OnFalse != Node::False)
                {
                    Solution_p* Duplicate = Solution->Clone();
                    Duplicate->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), true);
                    FindAllSolutions_Rec(Context, Node->OnTrue, Duplicate, Solutions);
                    Solution->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), false);
                    FindAllSolutions_Rec(Context, Node->OnFalse, Solution, Solutions);
                }
                else
                {
                    Solution->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), true);
                    FindAllSolutions_Rec(Context, Node->OnTrue, Solution, Solutions);
                }
            }
            else if (Node->OnFalse != Node::False)
            {
                Solution->SetState(Context->GetVariableName(POINTER_TO_U32(Node->Variable)), false);
                FindAllSolutions_Rec(Context, Node->OnFalse, Solution, Solutions);
            }
        }

        std::vector<Solution_p*> Function_p::FindAllSolutions() const
        {
            Node* bdd = _Context->GetMemoryPool()->GetNodeFromId(_BddId);
            std::vector<Solution_p*> solutions;
            if (bdd->Solutions == 0)
                return solutions;
            Solution_p* sol = new Solution_p();
            FindAllSolutions_Rec(_Context, bdd, sol, solutions);
            return solutions;
        }

        Function_p Function_p::Restrict(u32 Variable, bool Value) const
        {
            Node* bdd = _Context->GetMemoryPool()->GetNodeFromId(_BddId);
            return Function_p(_Context, bdd->Restrict(_Context, Variable, Value));
        }

        Function_p Function_p::And(const Function_p& Func) const
        {
            return Apply<LogicalFunctions::And>(Func);
        }

        Function_p Function_p::Or(const Function_p& Func) const
        {
            return Apply<LogicalFunctions::Or>(Func);
        }

        Function_p Function_p::Xor(const Function_p& Func) const
        {
            return Apply<LogicalFunctions::Xor>(Func);
        }

        bool Function_p::IsTautology() const
        {
            // Because we are using RoBdd no need to do something recursive
            Node* bdd = _Context->GetMemoryPool()->GetNodeFromId(_BddId);
            return bdd->OnTrue == Node::True && bdd->OnFalse == Node::True;
        }

        bool Function_p::IsSatisfiable() const
        {
            Node* bdd = _Context->GetMemoryPool()->GetNodeFromId(_BddId);
            return POINTER_TO_U32(bdd->Solutions) > 0;
        }

        u32 Function_p::SatCount() const
        {
            Node* bdd = _Context->GetMemoryPool()->GetNodeFromId(_BddId);
            return POINTER_TO_U32(bdd->Solutions);
        }

        static size_t Not_Rec(Context_p* ContextPtr, size_t NodeId)
        {
            if (NodeId == Node::IdTrue)
            {
                return Node::IdFalse;
            }

            if (NodeId == Node::IdFalse)
            {
                return Node::IdTrue;
            }

            Node* NodePtr = ContextPtr->GetMemoryPool()->GetNodeFromId(NodeId);
            size_t notOnFalse = Not_Rec(ContextPtr, ContextPtr->GetMemoryPool()->GetIdFromNode(NodePtr->OnFalse));

            // The pointer may have been invalidated reload it
            NodePtr = ContextPtr->GetMemoryPool()->GetNodeFromId(NodeId);
            size_t notOnTrue = Not_Rec(ContextPtr, ContextPtr->GetMemoryPool()->GetIdFromNode(NodePtr->OnTrue));

            // The pointer may have been invalidated reload it
            NodePtr = ContextPtr->GetMemoryPool()->GetNodeFromId(NodeId);
            return ContextPtr->AddNode(POINTER_TO_U32(NodePtr->Variable), notOnTrue, notOnFalse);
        }

        Function_p Function_p::Not() const
        {
            return Function_p(_Context, Not_Rec(_Context, _BddId));
        }

        Function_p Function_p::LogicalExist(const std::string& Variable) const
        {
            return LogicalExist(_Context->GetVariableId(Variable));
        }

        Function_p Function_p::LogicalExist(u32 Variable) const
        {
            return Restrict(Variable, true) | Restrict(Variable, false);
        }

        Function_p Function_p::Implies(const Function_p& Func) const
        {
            return Apply<LogicalFunctions::Implies>(Func);
        }

        Function_p Function_p::Equivalent(const Function_p& Func) const
        {
            return Xor(Func).Not();
        }

        Function_p Function_p::Replace(const std::string& Variable, const Function_p& Func) const
        {
            return Replace(_Context->GetVariableId(Variable), Func);
        }

        Function_p Function_p::Replace(u32 Variable, const Function_p& Func) const
        {
            // t[t'\x] = (t' && t[1\x]) || (!t' && t[0\x])
            return (Restrict(Variable, true).And(Func)).Or(Restrict(Variable, false).And(Func.Not()));
        }

        void Function_p::InplaceAnd(const Function_p& Func)
        {
            _BddId = Node::Apply<LogicalFunctions::And>(_Context, _BddId, Func._BddId);
        }

        void Function_p::InplaceOr(const Function_p& Func)
        {
            _BddId = Node::Apply<LogicalFunctions::Or>(_Context, _BddId, Func._BddId);
        }

        void Function_p::InplaceXor(const Function_p& Func)
        {
            _BddId = Node::Apply<LogicalFunctions::Xor>(_Context, _BddId, Func._BddId);
        }

        void Function_p::InplaceNot()
        {
            _BddId = Not_Rec(_Context, _BddId);
        }

        void Function_p::InplaceImplies(const Function_p& Func)
        {
            _BddId = Node::Apply<LogicalFunctions::Implies>(_Context, _BddId, Func._BddId);
        }

        void Function_p::InplaceEquivalent(const Function_p& Func)
        {
            _BddId = Node::Apply<LogicalFunctions::Xor>(_Context, _BddId, Func._BddId);
            Not_Rec(_Context, _BddId);
        }

        Function_p Function_p::operator &(const Function_p& Func) const
        {
            return And(Func);
        }

        Function_p Function_p::operator |(const Function_p& Func) const
        {
            return Or(Func);
        }

        Function_p Function_p::operator ^(const Function_p& Func) const
        {
            return Xor(Func);
        }

        Function_p Function_p::operator !() const
        {
            return Function_p(_Context, Not_Rec(_Context, _BddId));
        }

        Function_p& Function_p::operator &=(const Function_p& Func)
        {
            InplaceAnd(Func);
            return *this;
        }

        Function_p& Function_p::operator |=(const Function_p& Func)
        {
            InplaceOr(Func);
            return *this;
        }

        Function_p& Function_p::operator ^=(const Function_p& Func)
        {
            InplaceXor(Func);
            return *this;
        }

        bool Function_p::operator ==(const Function_p& Func)
        {
            return (_BddId == Func._BddId);
        }

        bool Function_p::operator !=(const Function_p& Func)
        {
            return (_BddId != Func._BddId);
        }

        static std::string ToDot_NodeName(Context_p* Context, Node* Node)
        {
            std::stringstream NodeNameStream;
            NodeNameStream << "_" << Context->GetMemoryPool()->GetIdFromNode(Node);
            return NodeNameStream.str();
        }

        static void ToDot_Rec(Context_p* Context, Node* Node, std::stringstream& stream, std::set<std::string>& forward)
        {

            std::string NodeName = ToDot_NodeName(Context, Node);
            forward.insert(NodeName + " [label=\"" + Context->GetVariableName(POINTER_TO_U32(Node->Variable)) + "\"]"); 
            if (Node->OnTrue == Node::True)
                stream << NodeName << " -> true;" << std::endl;
            else if (Node->OnTrue == Node::False)
                stream << NodeName << " -> false;" << std::endl;
            else
            {
                std::string OnTrueName = ToDot_NodeName(Context, Node->OnTrue);
                stream
                    << NodeName << " -> "
                    << OnTrueName<< ";"
                    << std::endl;

                ToDot_Rec(Context, Node->OnTrue, stream, forward);
            }

            if (Node->OnFalse == Node::True)
                stream << NodeName << " -> true [style=dotted];" << std::endl;
            else if (Node->OnFalse == Node::False)
                stream << NodeName << " -> false [style=dotted];" << std::endl;
            else
            {
                std::string OnFalseName = ToDot_NodeName(Context, Node->OnFalse);
                stream
                    << NodeName << " -> "
                    << OnFalseName << " [style=dotted];"
                    << std::endl;

                ToDot_Rec(Context, Node->OnFalse, stream, forward);
            }
        }

        std::string Function_p::ToDot() const
        {
            std::stringstream structure;
            std::stringstream stream;
            std::set<std::string> forward;

            ToDot_Rec(_Context, _Context->GetMemoryPool()->GetNodeFromId(_BddId), stream, forward);
            structure << "digraph bdd {" << std::endl;
            for (std::set<std::string>::iterator it = forward.begin();
                    it != forward.end();
                    it++)
            {
                structure << *(it) << ";" << std::endl;
            }
            structure << stream.str() << std::endl;
            structure << "}" << std::endl;

            return structure.str();
        }


        Function_p True(Context_p* ctx)
        {
            return Function_p(ctx, Node::True);
        }

        Function_p False(Context_p* ctx)
        {
            return Function_p(ctx, Node::False);
        }

    }
}
