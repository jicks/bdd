/*
 * File : Context_p.cpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include <iostream>
#include <sstream>

#include "../Platform.hpp"
#include "Bytecode.hpp"
#include "Context_p.hpp"

namespace BDD
{
    namespace Internal
    {
        Context_p::Context_p(u64 MaxMemorySize) :
            _MemoryPool(MaxMemorySize),
            _VariableId(),
            _VariableName(),
            _NodeMap(),
            _CachedOperations(0),
            _NextId(Bytecode::LastOpCode + 1)
        {
#ifdef WITH_CPP11
            _CachedOperations = new std::unordered_map<std::pair<NodeId, NodeId>, NodeId>[MAX_LOGICAL_FUNCTIONS];
#else
            _CachedOperations = new std::map<std::pair<NodeId, NodeId>, NodeId>[MAX_LOGICAL_FUNCTIONS];
#endif
        }

        Context_p::~Context_p()
        {
            if (_CachedOperations)
            {
                delete [] _CachedOperations;
                _CachedOperations = 0;
            }
        }

        bool Context_p::IsValid()
        {
            return true;
        }

        std::string Context_p::GetVariableName(u32 Id) const
        {
            if (!_VariableName.count(Id))
            {
                std::stringstream stream;
                stream << "Id" << Id;
                return stream.str();
            }
            return _VariableName.find(Id)->second;
        }

        u32 Context_p::GetVariableId()
        {
            u32 anonymousId = _NextId;
            _VariableName[_NextId] = "?";
            ++_NextId;
            return anonymousId;
        }

        u32 Context_p::AddNode(u32 VariableId, NodeId Lhs, NodeId Rhs)
        {
            std::pair<u32, std::pair<NodeId, NodeId> > key =
                std::make_pair(VariableId, std::make_pair(Lhs, Rhs));
#ifdef WITH_CPP11
            std::unordered_map<std::pair<u32, std::pair<NodeId, NodeId> >, NodeId>::iterator iterator;
#else
            std::map<std::pair<u32, std::pair<NodeId, NodeId> >, NodeId>::iterator iterator;
#endif
            iterator = _NodeMap.find(key);
            if (iterator != _NodeMap.end())
                return iterator->second;

            NodeId id =  _MemoryPool.AddNodeId(VariableId, Lhs, Rhs);
            _NodeMap.insert(std::make_pair(key, id));
            return id;
        }

        u32 Context_p::GetVariableId(const std::string& Name)
        {
            if (!_VariableId.count(Name))
            {
                _VariableId[Name] = _NextId;
                _VariableName[_NextId] = Name;
                ++_NextId;
            }
            return _VariableId[Name];
        }

        MemoryPool* Context_p::GetMemoryPool()
        {
            return &_MemoryPool;
        }

        NodeId Context_p::GetCachedResult(NodeId Lhs, NodeId Rhs, u8 Op) const
        {
#ifdef WITH_CPP11
            std::unordered_map<std::pair<NodeId, NodeId>, NodeId>::const_iterator iterator;
#else
            std::map<std::pair<NodeId, NodeId>, NodeId>::const_iterator iterator;
#endif
            if (Lhs > Rhs && Op < MAX_LOGICAL_COMMUTATIVE)
                iterator = _CachedOperations[Op].find(std::make_pair(Rhs, Lhs));
            else
                iterator = _CachedOperations[Op].find(std::make_pair(Lhs, Rhs));
            return (iterator != _CachedOperations[Op].end() ? iterator->second : 0);
        }

        void Context_p::SetCachedResult(NodeId Lhs, NodeId Rhs, u8 Op, NodeId Result)
        {
            if (Lhs > Rhs && Op < MAX_LOGICAL_COMMUTATIVE)
                _CachedOperations[Op].insert(std::make_pair(std::make_pair(Rhs, Lhs), Result));
            else
                _CachedOperations[Op].insert(std::make_pair(std::make_pair(Lhs, Rhs), Result));
        }
    }
}
