/*
 * File : Function.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include "Function.hpp"
#include "Platform.hpp"
#include "internal/Function_p.hpp"

namespace BDD
{
    namespace Internal
    {
        class Context;
    }
    Function::Function()
        : _Function(0),
          _Refcount(0)
    {
    }

    Function::Function(Context& Context)
        : _Function(0),
          _Refcount(0)
    {
        _Function = new Internal::Function_p(Context.Raw());
        _Refcount = new u32();
        *_Refcount = 1;
    }

    Function::Function(Context& Context, const char* Variable)
        : _Function(0),
          _Refcount(0)
    {
        _Function = new Internal::Function_p(Context.Raw(), Variable);
        _Refcount = new u32();
        *_Refcount = 1;
    }

    Function::Function(Internal::Function_p* Pointer)
        : _Function(Pointer),
          _Refcount(0)
    {
        if (Pointer)
        {
            _Refcount = new u32();
            *_Refcount = 1;
        }
    }

    Function::Function(const Function& Fun)
        : _Function(Fun._Function),
          _Refcount(Fun._Refcount)
    {
        if (_Refcount)
        {
            *_Refcount = *_Refcount + 1;
        }
    }

    Function::~Function()
    {
        if (_Refcount)
        {
            *_Refcount = *_Refcount - 1;
            if (*_Refcount == 0)
            {
                if (_Function)
                {
                    delete _Function;
                    _Function = 0;
                }
                delete _Refcount;
                _Refcount = 0;
            }
        }
    }

    Function Function::Clone() const
    {
        if (!_Function || !_Refcount)
            return Function();
        return Function(_Function->Clone());
    }

    Solution Function::FindOneSolution() const
    {
        return Solution(_Function->FindOneSolution());
    }


    SolutionList Function::FindAllSolutions() const
    {
        SolutionList list;
        std::vector<Internal::Solution_p*> solutions =
            _Function->FindAllSolutions();
        for (
                std::vector<Internal::Solution_p*>::const_iterator it =
                solutions.begin();
                it != solutions.end();
                it++)
        {
            list.Add(Solution(*it));
        }
        return list;
    }

    bool Function::IsValid() const
    {
        if (_Function && _Refcount)
        {
            return _Function->IsValid();
        }
        return false;
    }

    bool Function::IsTautology() const
    {
        return (IsValid()) &&  _Function->IsTautology();
    }

    bool Function::IsSatisfiable() const
    {
        return (IsValid()) &&  _Function->IsSatisfiable();
    }

    u32 Function::SatCount() const
    {
        if (!IsValid()) { return 0; }
        return _Function->SatCount();
    }

    Function Function::And(const Function& Func) const
    {
        if (!IsValid() || !Func.IsValid())
        {
            if (Func.IsValid())
                return Func;
            else if (IsValid())
                return *this;
            return Function(0);
        }
        return Function(_Function->And(*Func._Function).Clone());
    }

    Function Function::Or(const Function& Func) const
    {
        if (!IsValid() || !Func.IsValid())
        {
            if (IsValid())
                return Function(this->_Function->Clone());
            else if (Func.IsValid())
                return Function(Func._Function->Clone());
            return Function(0);
        }
        return Function(_Function->Or(*Func._Function).Clone());
    }

    Function Function::Xor(const Function& Func) const
    {
        if (!IsValid() || !Func.IsValid())
        {
            if (IsValid())
                return Function(this->_Function->Clone());
            else if (Func.IsValid())
                return Function(Func._Function->Clone());
            return Function(0);
        }
        return Function(_Function->Xor(*Func._Function).Clone());
    }

    Function Function::Not()
    {
        if (!IsValid())
        {
            return Function(0);
        }
        return Function(_Function->Not().Clone());
    }

    Function Function::LogicalExist(const char* Variable) const
    {
        if (!IsValid())
        {
            return Function(0);
        }
        return Function(_Function->LogicalExist(Variable).Clone());
    }

    Function Function::Implies(const Function& Func) const
    {
        if (!IsValid() || !Func.IsValid())
        {
            if (IsValid())
                return Function(this->_Function->Clone());
            else if (Func.IsValid())
                return Function(Func._Function->Clone());
            return Function(0);
        }
        return Function(_Function->Implies(*Func._Function).Clone());
    }

    Function Function::Equivalent(const Function& Func) const
    {
        if (!IsValid() || !Func.IsValid())
        {
            return Function(0);
        }
        return Function(_Function->Equivalent(*Func._Function).Clone());
    }

    Function Function::Replace(const char* Variable, const Function& Func) const
    {
        if (!IsValid() || !Func.IsValid())
        {
            return Function(0);
        }
        return Function(_Function->Replace(Variable, *Func._Function).Clone());
    }

    void Function::InplaceAnd(const Function& Func)
    {
        if (!IsValid() || !Func.IsValid())
        {
            if (Func.IsValid())
            {
                _Function = Func._Function->Clone();
                _Refcount = new u32();
                if (_Refcount)
                    *_Refcount = 1;
            }
            return;
        }

        _Function->InplaceAnd(*Func._Function);
    }

    void Function::InplaceOr(const Function& Func)
    {
        if (!IsValid() || !Func.IsValid())
        {
            if (Func.IsValid())
            {
                _Function = Func._Function->Clone();
                _Refcount = new u32();
                if (_Refcount)
                    *_Refcount = 1;
            }
            return;
        }

        _Function->InplaceOr(*Func._Function);
    }

    void Function::InplaceXor(const Function& Func)
    {
        if (!IsValid() || !Func.IsValid())
        {
            if (Func.IsValid())
            {
                _Function = Func._Function->Clone();
                _Refcount = new u32();
                if (_Refcount)
                    *_Refcount = 1;
            }
            return;
        }

        _Function->InplaceXor(*Func._Function);
    }

    void Function::InplaceNot()
    {
        if (!IsValid())
        {
            return;
        }

        _Function->InplaceNot();
    }

    Function Function::operator &(const Function& Func) const
    {
        if (!IsValid() || !Func.IsValid())
        {
            if (IsValid())
                return Function(this->_Function->Clone());
            else if (Func.IsValid())
                return Function(Func._Function->Clone());
            return Function(0);
        }

        return Function(_Function->operator &(*Func._Function).Clone());
    }

    Function Function::operator |(const Function& Func) const
    {
        if (!IsValid() || !Func.IsValid())
        {
            if (IsValid())
                return Function(this->_Function->Clone());
            else if (Func.IsValid())
                return Function(Func._Function->Clone());
            return Function(0);
        }

        return Function(_Function->operator |(*Func._Function).Clone());
    }

    Function Function::operator ^(const Function& Func) const
    {
        if (!IsValid() || !Func.IsValid())
        {
            if (IsValid())
                return Function(this->_Function->Clone());
            else if (Func.IsValid())
                return Function(Func._Function->Clone());
            return Function(0);
        }

        return Function(_Function->operator ^(*Func._Function).Clone());
    }

    Function Function::operator !() const
    {
        if (!IsValid())
        {
            return Function(0);
        }

        return Function(_Function->operator !().Clone());
    }

    Function& Function::operator &=(const Function& Func)
    {
        if (!IsValid() || !Func.IsValid())
        {
            if (IsValid())
                return *this;
            else if (Func.IsValid())
            {
                if (Func.IsValid())
                {
                    _Function = Func._Function->Clone();
                    _Refcount = new u32();
                    if (_Refcount)
                        *_Refcount = 1;
                }				if (Func.IsValid())
                {
                    _Function = Func._Function->Clone();
                    _Refcount = new u32();
                    if (_Refcount)
                        *_Refcount = 1;
                }
            }
            return *this;
        }
        _Function->InplaceAnd(*(Func._Function));
        return *this;
    }

    Function& Function::operator |=(const Function& Func)
    {
        if (!IsValid() || !Func.IsValid())
        {
            if (IsValid())
                return *this;
            else if (Func.IsValid())
            {
                if (Func.IsValid())
                {
                    _Function = Func._Function->Clone();
                    _Refcount = new u32();
                    if (_Refcount)
                        *_Refcount = 1;
                }
            }
            return *this;
        }

        _Function->InplaceOr(*(Func._Function));
        return *this;
    }

    Function& Function::operator ^=(const Function& Func)
    {
        if (!IsValid() || !Func.IsValid())
        {
            if (IsValid())
                return *this;
            else if (Func.IsValid())
            {
                if (Func.IsValid())
                {
                    _Function = Func._Function->Clone();
                    _Refcount = new u32();
                    if (_Refcount)
                        *_Refcount = 1;
                }
            }
            return *this;
        }

        _Function->InplaceXor(*(Func._Function));

        return *this;
    }

    bool Function::operator ==(const Function& Func)
    {
        if (!_Function)
            return !Func._Function;
        if (!Func._Function)
            return false;
        return (*_Function) == (*Func._Function);
    }

    bool Function::operator !=(const Function& Func)
    {
        if (!_Function)
            return Func._Function;
        if (!Func._Function)
            return true;
        return (*_Function) != (*Func._Function);
    }

    Function& Function::operator =(const Function& Func)
    {
        if (_Refcount)
        {
            *_Refcount = *_Refcount - 1;
            if (*_Refcount == 0)
            {
                if (_Function)
                {
                    delete _Function;
                }
                delete _Refcount;
            }
        }
        _Function = Func._Function;
        _Refcount = Func._Refcount;
        if (_Refcount)
        {
            *_Refcount = *_Refcount + 1;
        }
        else
        {
            _Function = 0;
        }
        return *this;
    }


    void* Function::operator new(size_t size)
    {
        void* arr = malloc(size * sizeof(Function));
        memset(arr, 0, size * sizeof(Function));
        return arr;
    }

    void Function::operator delete(void* ptr)
    {
        if (ptr)
        {
            free(ptr);
        }
    }

    void* Function::operator new[](size_t size)
    {
        void* arr = malloc(size * sizeof(Function));
        memset(arr, 0, size * sizeof(Function));
        return arr;
    }

    void Function::operator delete[](void* p)
    {
        if (p)
        {
            free(p);
        }
    }

    std::string Function::ToDot() const
    {
        if (!IsValid())
        {
            return "";
        }

        return _Function->ToDot();
    }

    Function True(Context Ctx)
    {
        return Function(Internal::True(Ctx.Raw()).Clone());
    }
    Function False(Context Ctx)
    {
        return Function(Internal::False(Ctx.Raw()).Clone());
    }

}
