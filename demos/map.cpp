#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <string>
#include <cstring>
#include <utility>
#include <Context.hpp>
#include <Function.hpp>

class State
{
  public:
    State(BDD::Context& ctx, const std::string name)
      : _State(name)
    {
      std::stringstream ssa;
      ssa << name << "_0";
      _AName = ssa.str();
      _AFun = BDD::Function(ctx, _AName.c_str());
      std::stringstream ssb;
      ssb << name << "_1";
      _BName = ssb.str();
      _BFun = BDD::Function(ctx, _BName.c_str());
    }

    std::string GetColor(BDD::Solution sol)
    {
      bool aState = sol.GetState(_AName.c_str());
      bool bState = sol.GetState(_BName.c_str());
      if (aState && bState) return _State + " red";
      if (aState && !bState) return _State + " green";
      if (!aState && bState) return _State + " blue";
      return _State + " yellow";
    }

    BDD::Function operator !=(const State& state)
    {
      return (_AFun ^ state._AFun) | (_BFun ^ state._BFun);
    }

  private:
    std::string _State;
    std::string _AName;
    BDD::Function _AFun;
    std::string _BName;
    BDD::Function _BFun;
};

static std::vector<std::pair<std::string, std::string> > linksFromFile(const std::string& file)
{
  std::vector<std::pair<std::string, std::string> > links;
  std::ifstream input(file.c_str());
  while (!input.eof() && !input.bad())
  {
    std::string left = "";
    std::string right = "";
    input >> left;
    input >> right;
    if (left != "" && right != "")
      links.push_back(std::make_pair(left, right));
  }
  std::cout << "links found : " << links.size() <<std::endl;
  return links;
}

static void mapSolver(const std::vector<std::pair<std::string, std::string> >& links)
{
  BDD::Context ctx;
  BDD::Function result;
  std::map<std::string, State> states;

  for (
      std::vector<std::pair<std::string, std::string> >::const_iterator it = links.begin();
      it != links.end();
      it++)
  {
    // Get the first state
    std::map<std::string, State>::iterator stleft =
      states.find(it->first);
    if (stleft == states.end())
    {
      states.insert(std::make_pair(it->first, State(ctx, it->first)));
      stleft = states.find(it->first);
    }
    // Get the second state
    std::map<std::string, State>::iterator stright =
      states.find(it->second);
    if (stright == states.end())
    {
      states.insert(std::make_pair(it->second, State(ctx, it->second)));
      stright = states.find(it->second);
    }

    // If two state are linked they must not have the same color
    std::cout << "add links (" << it->first << ", " << it->second << ")" <<std::endl;
    result &= (stleft->second != stright->second);
  }

  BDD::Solution solution = result.FindOneSolution();

  std::cout << "one solution is :" << std::endl;
  for (
      std::map<std::string, State>::iterator it = states.begin();
      it != states.end();
      it++)
  {
    std::cout << it->second.GetColor(solution) << std::endl;
  }
}

static std::string usage(std::string name)
{
  return name + " is a graph coloration solver.\n"
    + "You must provide a file containing the links between the nodes.";
}

int main(int argc, char** argv)
{
  if (argc != 2)
  {
    std::cout << usage(argv[0]) << std::endl;
    return 1;
  }
  else
  {
    if (!strcmp(argv[1], "--help"))
    {
      std::cout << usage(argv[0]) << std::endl;
      return 0;
    }
    else
    {
      mapSolver(linksFromFile(argv[1]));
    }
  }
  return 0;
}
